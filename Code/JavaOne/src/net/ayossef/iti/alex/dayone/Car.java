package net.ayossef.iti.alex.dayone;

public class Car {
    String model;
    String color;
    String brand;
    int speed;
    static int maxSpeed = 120;

    public static void main(String[] args) {

    }

    public Car(){}
    public Car(String model, String color, String brand) {
        this.model = model;
        this.color = color;
        this.brand = brand;
        this.speed = 0;
    }

    public void speedUp(){
        this.speed += 20;
        // doesn't exceed the max speed
    }
    public void slowDown(){
        this.speed -= 20;
        // doesn't go beyond zero
    }

    public void printStatus(){
        // tell about the car
    }

  @Override
  public String toString() {
    return "Car{" +
      "model='" + model + '\'' +
      ", color='" + color + '\'' +
      ", brand='" + brand + '\'' +
      ", speed=" + speed +
      '}';
  }
}
