package net.ayossef.iti.alex.daytwo.task;

// Data Holder
public class Meal {
    // name
    private String name;
    // number of cal
    private int cals;
    // price
    private int price;
    // type (food - drink)
    private boolean isFood;


  public Meal(String name, int cals, int price, boolean isFood) {
    this.name = name;
    this.cals = cals;
    this.price = price;
    this.isFood = isFood;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getCals() {
    return cals;
  }

  public void setCals(int cals) {
    this.cals = cals;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public boolean isFood() {
    return isFood;
  }

  public void setFood(boolean food) {
    isFood = food;
  }

  @Override
  public String toString() {
    return "Meal{" +
      "name='" + name + '\'' +
      ", cals=" + cals +
      ", price=" + price +
      ", isFood=" + isFood +
      '}';
  }

  public String toShortString(){
    return "Name: "+this.name+" has price "+this.price;
  }
}
