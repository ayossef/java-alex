package net.ayossef.iti.alex.daytwo;

import java.util.ArrayList;

public class Student {
    private String name;
    private String email;
    private int id;

    ArrayList<Course> courses = new ArrayList<>();
    public void register(Course newCourse){
        if (courses.size() > 4){
            return;
        }else {
            courses.add(newCourse);
        }
    }
    public Student(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
