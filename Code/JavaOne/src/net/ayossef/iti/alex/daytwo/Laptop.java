package net.ayossef.iti.alex.daytwo;

public class Laptop {
    private int screenSize;
    private int processorFreq;
    private int processorCores;
    private int diskSpace;
    private int ramSpace;

    public Laptop(int screenSize){
        this.screenSize = screenSize;
    }


    public int getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(int screenSize) {
        this.screenSize = screenSize;
    }

    public int getProcessorFreq() {
        return processorFreq;
    }

    public void setProcessorFreq(int processorFreq) {
        this.processorFreq = processorFreq;
    }

    public int getProcessorCores() {
        return processorCores;
    }

    public void setProcessorCores(int processorCores) {
        this.processorCores = processorCores;
    }

    public int getDiskSpace() {
        return diskSpace;
    }

    public void setDiskSpace(int diskSpace) {
        this.diskSpace = diskSpace;
    }

    public int getRamSpace() {
        return ramSpace;
    }

    public void setRamSpace(int ramSpace) {
        this.ramSpace = ramSpace;
    }


}
