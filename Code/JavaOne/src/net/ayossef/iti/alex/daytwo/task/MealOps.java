package net.ayossef.iti.alex.daytwo.task;

import java.lang.reflect.Array;
import java.util.ArrayList;

// Operational Class
public class MealOps {
    // list of meals
  ArrayList<Meal> menu = new ArrayList<>();

    // get cheap meals < 50
  public ArrayList<Meal> getCheapMeals(){
    int priceLimit = 50;
    ArrayList<Meal> cheapMeals = new ArrayList<>();
    for (Meal currentMeal:menu) {
      int currentMealPrice = currentMeal.getPrice();
      if(currentMealPrice < priceLimit){
        cheapMeals.add(currentMeal);
      }
    }
    return cheapMeals;
  }

    // get health meals < 70
  public ArrayList<Meal> getHealthyMeals(){
    return null;
  }

    // print all meals
  public void printMenu(){
    for (Meal currentMeal:menu) {
      System.out.println(currentMeal);
    }
  }

    // add new meal
  public void addMeal(Meal newMeal){
    this.menu.add(newMeal);
  }

    // find meal by name
  public Meal findByName(String nameToFind){
    // Not Found
    // 1. Return null
    // 2. Throw Exception
    for (Meal currentMeal :menu) {
      String currentMealName = currentMeal.getName();
      if(currentMealName.equalsIgnoreCase(nameToFind)){
        return currentMeal;
      }
    }
    return null;
  }
  public void removeMealByName(String name){

  }
  // increase the price by 20%
  public void el7a2Elta3wem(){

  }
  // decrease the cal by 20%
  public void otob5Be3afya(){

  }
}
