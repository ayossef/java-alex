package net.ayossef.iti.alex.daytwo;

public class Main {
    public static void main(String[] args) {
        System.out.println("Welcome to Day 2");
        Laptop lenovoLaptop = new Laptop(13);
        Course dbCourse = new Course();
        dbCourse.courseId = 315;
        dbCourse.courseName = "Introduction to MySQL DB";
        dbCourse.instructorName = "Eng. Hasan";

        Student ali = new Student("Ali Mohamed", 98273);
        Student samir = new Student("Samir Mohamed", 98274);

        dbCourse.register(samir);
        dbCourse.register(ali);

        // 1. get list -> loop -> print

        // 2. call printRegisteredStudents

        // 3. create an object of CourseManager -> call print function
        CourseManager ostazMohamed = new CourseManager();
        ostazMohamed.printRegisteredStudentsInCourse(dbCourse);


    }
}
