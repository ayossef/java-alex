package eg.gov.iti.qa.javacourse;

import eg.gov.iti.qa.javacourse.cars.Car;
import eg.gov.iti.qa.javacourse.cars.SuperCar;
import eg.gov.iti.qa.javacourse.encapsulation.UserData;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        UserData userOne = new UserData();
        userOne.setName("First User");
        userOne.setAge(30);

        System.out.println(userOne);
    }
}