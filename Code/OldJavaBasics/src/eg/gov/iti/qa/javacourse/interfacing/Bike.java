package eg.gov.iti.qa.javacourse.interfacing;

public class Bike implements Ride{
    private int rpm;
    private int level;

    private int speed;

    @Override
    public void speedUp() {
        this.speed += 10;
    }

    @Override
    public void slowDown() {
        this.speed -= 5;
    }

    @Override
    public void showSpeed() {
        System.out.println("Speed is "+this.speed +" @ RPM "+this.rpm);
    }
}
