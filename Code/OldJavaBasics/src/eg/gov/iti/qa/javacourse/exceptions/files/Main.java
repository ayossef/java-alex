package eg.gov.iti.qa.javacourse.exceptions.files;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.WriteAbortedException;

public class Main {
    public static void main(String[] args) throws IOException {
        FileOperations fileOps = new FileOperations();
        try{
            fileOps.copyFile("dataSrc.txt", "dataDest.txt");
            throw new NullPointerException();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
