package eg.gov.iti.qa.javacourse.interfacing;

public abstract class Boat implements Ride{

    @Override
    public void slowDown() {

    }

    @Override
    public void showSpeed() {

    }
}
