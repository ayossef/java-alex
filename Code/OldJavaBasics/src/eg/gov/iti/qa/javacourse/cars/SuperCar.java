package eg.gov.iti.qa.javacourse.cars;

public class SuperCar extends Car{

    @Override
    public void speedUp() {
        this.speed += 40;
    }
}
