package eg.gov.iti.qa.javacourse.encapsulation;

public class UserData {
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age  <  0){
            this.age  = 0;
            return;
        }
        this.age = age;
    }

    private String firstName;
    private String lastName;

    public void setName(String name){
        String[] parts = name.split(" ");
        this.firstName = parts[0];
        this.lastName = parts[1];
    }
    public String getName(){
        return this.firstName+" "+this.lastName;
    }

    @Override
    public String toString() {
        return "UserData{" +
                "age=" + age +
                ", name='" + this.getName() + '\'' +
                '}';
    }
}
