package eg.gov.iti.qa.javacourse.exceptions;

public class MathOpOutput {
    boolean success;
    int result;

    public MathOpOutput(boolean success, int result) {
        this.success = success;
        this.result = result;
    }

    @Override
    public String toString() {
        return "MathOpOutput{" +
                "success=" + success +
                ", result=" + result +
                '}';
    }
}
