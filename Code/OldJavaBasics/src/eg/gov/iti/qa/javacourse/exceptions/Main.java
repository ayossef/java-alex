package eg.gov.iti.qa.javacourse.exceptions;

public class Main {
    public static void main(String[] args) {
        Calculator myCalc = new Calculator();
        System.out.println(myCalc.divide(10,0));

        try {
            System.out.println(myCalc.mod(10, 0));
        }catch(ArithmeticException exception){
            System.out.println("You can't Mod By Zero");
        }

        System.out.println(myCalc.add(30,20));
        System.out.println(myCalc.divide(12,4));
    }
}
