package eg.gov.iti.qa.javacourse.interfacing;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Bike myBike = new Bike();
        myBike.speedUp();
        myBike.speedUp();
        myBike.slowDown();
        System.out.println(myBike);

        ArrayList<Ride> differentRides = new ArrayList<>();
        differentRides.add(new Car());
        differentRides.add(new Bike());

        for (Ride myRide : differentRides) {
            myRide.speedUp();
            myRide.speedUp();
            myRide.slowDown();
            myRide.showSpeed();
        }
        Car firstRide = (Car) differentRides.get(0);
        firstRide.park();
        firstRide.showSpeed();

        Bike secondRide =  (Bike) differentRides.get(1);
        secondRide.speedUp();
        secondRide.showSpeed();

        new Button() {
            @Override
            public void onClick() {

            }
        };

        new Ride() {
            @Override
            public void speedUp() {

            }

            @Override
            public void slowDown() {

            }

            @Override
            public void showSpeed() {

            }
        };
    }
}
