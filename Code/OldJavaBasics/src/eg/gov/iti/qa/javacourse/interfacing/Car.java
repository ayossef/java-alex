package eg.gov.iti.qa.javacourse.interfacing;

public class Car implements Ride {
    protected int speed;
    protected String brand;

    public Car() {

    }

    public void speedUp(){
        this.speed += 20;
    }
    public void slowDown(){
        this.speed -= 20;
    }

    @Override
    public void showSpeed() {
        System.out.println("My Car Speed is "+speed);
    }

    public Car(int speed, String brand) {
        this.speed = speed;
        this.brand = brand;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Car{" +
                "speed=" + speed +
                ", brand='" + brand + '\'' +
                '}';
    }

    public void park(){
        this.speed = 0;
    }
}
