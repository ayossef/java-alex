package eg.gov.iti.qa.javacourse.exceptions;

public class Calculator {
    public int add(int paramOne, int paramTwo){
        return paramOne + paramTwo;
    }
    public MathOpOutput divide(int paramOne, int paramTwo){
        if(paramTwo == 0){
            System.out.println("ERROR MSG: You can't divide by zero");
            return new MathOpOutput(false,0);
        }
        return new MathOpOutput(true, paramOne/paramTwo);
    }
    public int mod(int paramOne, int paramTwo){
        return paramOne%paramTwo;
    }

}
