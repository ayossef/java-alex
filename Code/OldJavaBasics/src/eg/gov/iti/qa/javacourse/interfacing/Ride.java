package eg.gov.iti.qa.javacourse.interfacing;

public interface Ride {
    public void speedUp();
    public void slowDown();

    public void showSpeed();
}
