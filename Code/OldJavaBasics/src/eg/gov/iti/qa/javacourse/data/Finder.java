package eg.gov.iti.qa.javacourse.data;

import java.util.ArrayList;

public class Finder {
    public void find(String[] items, String item){
        boolean itemIsFound = false;

        for (int i = 0; i < items.length; i++) {
            String currentItem = items[i];
            if(currentItem == item){
                itemIsFound = true;
            }
        }
        if(itemIsFound){
            System.out.println("Item is found");
        } else{
            System.out.println("Item is not found");
        }
    }

    public boolean isFound(String[] items, String item){
        return true;
    }

    public int getCountOfItemInList(String[] items, String item){
        return 0;
    }

    public ArrayList<Integer> getLocationsOfItemInList(String[] items, String item){
        // Define Variables - ArryList, current Item, counter inside the loop
        ArrayList<Integer> locationsOfItem = new ArrayList<>();
        String currentItem ;
        // Loop
        for (int index = 0; index < items.length; index++) {
            // compare - if match - add the current index to the arraylist
            currentItem = items[index];
            if(currentItem == item){
                locationsOfItem.add(index);
            }
        }
        return locationsOfItem;
    }

    public int getCountOfWordInFile(String fileName, String word) throws Exception{
        boolean fileIsNotFound = true;
        if(fileIsNotFound){
            throw new Exception();
        }
        return 0;
    }
}
