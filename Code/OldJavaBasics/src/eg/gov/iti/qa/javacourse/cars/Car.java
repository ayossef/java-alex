package eg.gov.iti.qa.javacourse.cars;

public class Car {
// getters and setter for color and owner name
    // or using constructor
     int speed;

     String color;

     String ownerName;
     int maxSpeed = 120;

    public void speedUp(){
        this.speed += 20;
        if(this.speed >= this.maxSpeed){
            this.speed = this.maxSpeed;
        }
    }

    public void slowDown(){
        this.speed -= 20;
        if(this.speed < 0){
            this.speed = 0;
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "speed=" + speed +
                ", color='" + color + '\'' +
                ", ownerName='" + ownerName + '\'' +
                '}';
    }
}
